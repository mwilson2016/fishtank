export class Fish {
    constructor() {
        return this.data;
    }
    data: any = [{
        name: "american_crayfish",
        color: "black"
    },
    {
        name: "barreleyebattered_cod",
        color: "yellow"
    },
    {
        name: "betta_splendens",
        color: "red"
    },
    {
        name: "bonnethead",
        color: "blue"
    },
    {
        name: "cichlids",
        color: "lightblue"
    },
    {
        name: "cleaner_shrimp",
        color: "green"
    },
    {
        name: "cocoa_damselfish",
        color: "white"
    },
    {
        name: "coelocanth",
        color: "lightyellow"
    },
    {
        name: "cookiecutter",
        color: "lightgreen"
    },
    {
        name: "cuttlefish",
        color: "purple"
    },
    {
        name: "damselfish",
        color: "darkpurple"
    },
    {
        name: "dragon_wrasse",
        color: "darkgreen"
    },
    {
        name: "electrophorus",
        color: "darkblue"
    },
    {
        name: "elephant_seal",
        color: "darkred"
    },
    {
        name: "elvers",
        color: "darkyellow"
    },
    {
        name: "fanfin_seadevil",
        color: "lightpurple"
    },
    {
        name: "fish_fingers",
        color: "azure"
    },
    {
        name: "french_angel_fish",
        color: "orange"
    },
    {
        name: "hammerhead",
        color: "darkorange"
    },
    {
        name:  "harlequin_shrimp",
        color: "lightorange"
    },
    {
        name: "hawksbill_turtle",
        color: "beige"
    },
    {
        name: "megalodon",
        color: "brown"
    },
    {
        name: "minnow",
        color: "lightbrown"
    },
    {
        name: "neon_tetra",
        color: "dark brown"
    },
    {
        name: "oarfish",
        color: "chocolate"
    },
    {
        name: "painted_lobster",
        color: "pink"
    },
    {
        name: "prawn_cocktail",
        color: "cadetblue"
    },
    {
        name: "psychedelic_frogfish",
        color: "salmon"
    },
    {
        name: "robocod",
        color: "lightsalmon"
    },
    {
        name: "salmon_shark",
        color: "darksalmon"
    },
    {
        name: "sand_eel",
        color: "palevioletred"
    },
    {
        name: "sea_lion",
        color: "burlywood"
    },
    {
        name: "shortfin_mako_shark",
        color: "darkolivegreen"
    },
    {
        name: "slipper_lobster",
        color: "lemonchiffon"
    },
    {
        name: "sockeye_salmon",
        color: "slategrey"
    },
    {
        name: "spanish_hogfish",
        color: "darkgrey"
    },
    {
        name: "spinner_dolphin",
        color: "lightgrey"
    }];
}