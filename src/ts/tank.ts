import { Fish } from './fish';

class Tank {
    /*
     * 
     * Data to be sent to the API 
     * 
     */
    data: any = {
        fish: []
    }
    /*
     * 
     * Data to be sent to the API 
     * 
     */
    text: any = {
        select: "Select a fish to buy",
        error: "Error, please try again",
        notCompatible: " is not compatible in your tank"
    }
    /*
    * 
    * Delay between error messages
    * 
    */
    delayAmount: Number = 2000;
    constructor() {
        /*
        * 
        * Load all fish
        * 
        */
        this.loadFish();
        /*
        * 
        * Observe the fish in the tank, 
        * 
        */
        this.observeFish();
    }
    /*
    * 
    * Simple request method for calling the API
    * 
    */
    request(obj: any) {
        /*
        * 
        * Make the request a Promise
        * 
        */
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open(obj.method || "POST", obj.url);
            if (obj.headers) {
                Object.keys(obj.headers).forEach(key => {
                    xhr.setRequestHeader(key, obj.headers[key]);
                });
            }
            xhr.onload = () => {
                /*
                * 
                * If the request is successful, resolve the promise
                * 
                */
                if (xhr.status >= 200 && xhr.status < 300) {
                    resolve(xhr.response);
                } else {
                    /*
                    * 
                    * Reject the promise a provoke an error
                    * 
                    */
                    reject(xhr.statusText);
                }
            };
            xhr.onerror = () => reject(xhr.statusText);
            xhr.send(obj.body);
        });
    }
    /*
    * 
    * Observe the fish in the shop tank
    * 
    */
    loadFish() {
        var tank = document.querySelector('.js-shop-tank');
        var fishes: any = new Fish();

        fishes.forEach((item: any) => {
            tank.innerHTML += '<div class="fish js-animation-swim-' + this.getRandomSwimAnimation() + '" data-fish="' + item.name + '" style="background: ' + item.color + ';"></div>';
        });
    }
    getRandomSwimAnimation() {
        console.log("fired");
        return (Math.floor(Math.random() * 4) + 1);
    }
    /*
    * 
    * Observe the fish in the shop tank
    * 
    */
    observeFish() {
        /*
        * 
        * Get all fish in the shop tank
        * 
        */
        var fishes = document.querySelectorAll('.js-shop-tank .fish');
        /*
        * 
        * Listen for when a fish is clicked
        * 
        */
        Array.from(fishes).forEach(fish => {
            fish.addEventListener('click', (event) => {
                /*
                * 
                * Get the fish's name
                * 
                */
                var name = fish.getAttribute("data-fish");
                /*
                * 
                * Check the compatibility of the fish
                * 
                */
                this.compatibility(name);
            });
        });
    }
    /*
    * 
    * Check the compatibility of a group of fish
    * 
    */
    compatibility(name: String) {
        /*
        * 
        * Request the API
        * 
        */
        var toSend = this.data;

        toSend.fish.push(name);

        this.request({
            url: "https://fishshop.attest.tech/compatibility",
            method: "post",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(toSend)
        })
        .then((result: any) => {
            var buyButton = document.querySelectorAll(".js-buy-button")[0];
            result = JSON.parse(result);
            /*
            * 
            * If the fish can live together
            * 
            */
            if(result.canLiveTogether === true) {
                /*
                * 
                * Activate buy button
                * 
                */
                this.data.fish.push(name);

                buyButton.innerHTML = "Buy " + name;
                buyButton.className += " button--cta";

                buyButton.addEventListener('click', (event) => {
                    if(document.querySelector('.js-shop-tank .fish[data-fish="' + name + '"]')) {
                        /*
                        * 
                        * Move the fish into your tank
                        * 
                        */
                        document.querySelector('.js-shop-tank .fish[data-fish="' + name + '"]').remove();
                        document.querySelector(".js-my-tank").innerHTML += '<div class="fish js-animation-swim-' + this.getRandomSwimAnimation() + '" data-fish="' + name + '"></div>';
                        /*
                        * 
                        * Reset the button
                        * 
                        */
                        buyButton.innerHTML = this.text.select;
                        buyButton.className = buyButton.className.split(" button--cta")[0];
                    }
                });
            } else {
                buyButton.innerHTML = name + this.text.notCompatible;
                buyButton.className += " button--error";

                setTimeout(() => {
                    buyButton.innerHTML = this.text.select;
                    buyButton.className = "js-buy-button button--block";
                }, this.delayAmount);
            }
        })
        .catch(error => {
            /*
            * 
            * Make the buy button show an error because of the API
            * 
            */
            var buyButton = document.querySelector(".js-buy-button");

            buyButton.innerHTML = this.text.error;
            buyButton.className += " button--error";

            setTimeout(() => {
                buyButton.innerHTML = "Buy " + name;
                buyButton.className = "js-buy-button button--block button--cta";
            }, this.delayAmount);
        });
    }
}
/*
* 
* Initialise the Tank
* 
*/
new Tank();