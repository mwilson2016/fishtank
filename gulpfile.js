const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const ts = require('gulp-typescript');
const tsProject = ts.createProject('tsconfig.json');
const sass = require('gulp-sass');
const htmlmin = require('gulp-htmlmin');
const gulpWebpack = require('gulp-webpack');

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', () => {
    return gulp.src("src/scss/*.scss")
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest("dist/css"))
        .pipe(browserSync.stream());
});
// Compile TypeScript into Javascript
gulp.task('js', () => {
    var tsResult = gulp.src('src/ts/*.ts')
    .pipe(tsProject());
 
    return tsResult.js.pipe(gulp.dest('./dist/js')).pipe(browserSync.stream());
});
//
gulp.task('webpack', ['js'], () => {
    return gulp.src('dist/js/*.js')
        .pipe(gulpWebpack({
            output: {
                filename: "app.js",
            }
        }))
        .pipe(gulp.dest('dist/js/bundled'))
        .pipe(browserSync.stream());
});
gulp.task('html', () => {
  return gulp.src('src/index.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'))
    .pipe(browserSync.stream());
}); 
// Static Server + watching scss/html files
gulp.task('serve', ['webpack', 'sass', 'html'], () => {
    browserSync.init({
        server: "./dist"
    });
    gulp.watch("src/ts/*.ts", ['webpack']);
    gulp.watch("src/scss/*.scss", ['sass']);
    gulp.watch("src/index.html", ['html']);
});
//
gulp.task('default', ['serve']);